// import logo from './logo.svg';
import './App.css';
import React, {  useState } from 'react';
import UserName from './Validation/Username';
import UserList from './Validation/UserList';

// in this we are using ref and portal and fragemnts...
// ref to read the keystroke
//portal to render the dom elements in specified id
//Fragement to wrapper the contents...

function App() {

  const [userData,setUserData] = useState([]);
  
  const sendUserHandler = (uname) => {
    setUserData((prevdata) => {
    return  [...prevdata ,{name:uname , id:Math.random().toString() }];

    });
  };

  return(
  <React.Fragment>
   
    <UserName onSendUser = {sendUserHandler}/>
    <UserList items={userData}/>
         
    </React.Fragment>
  );
  
}

export default App;
