import React from 'react';
import Card from './Card'
import classes from './Error.module.css';
import ReactDOM from 'react-dom';

const ErrorModal = (props) => 
{
    const Backdrop = () => {
        return(
            <div className={classes.backdrop} onClick={props.onConfirm}/> 
        );
    }
    const Modal = () => {
        return(
            <Card className={classes.modal}>
            
            <header className={classes.header}>
                {props.title}
            </header>
            <div className={classes.content}>
                {props.message}
            </div>
            <footer className={classes.actions}>
                <button onClick={props.onConfirm}>Okay</button>
            </footer>
       
       
        </Card>
        );
    }
    return(
        <div>
           {
               ReactDOM.createPortal(<Backdrop onClick={props.onConfirm}/>,document.getElementById('backdrop-root'))
           }
           {
               ReactDOM.createPortal(<Modal title={props.title} message={props.message} onClick={props.onConfirm} />,document.getElementById('overlay-root'))
           }
        </div>
       
    );


}
export default ErrorModal;