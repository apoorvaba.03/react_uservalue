import React, { useState,useRef } from 'react';
import Card from '../UI/Card';
import ErrorModal from '../UI/ErrorModal';
import classes from './UserName.module.css';
const UserName = (props) => 
{
    const [inputUser,setInputUser] = useState('');
    const [userError,setError] = useState();
    const userInput=useRef();

    const addhandler =(event) => {
        event.preventDefault();
     //   console.log(inputUser);
    // console.log(userInput.current.value);
    const enterdvalue =userInput.current.value; 

       if(enterdvalue.trim().length === 0){
           setError(
               {
                   title:'Invallid input',
                   message:'please enter the valid input'
               }
           );
           return;
        }


        props.onSendUser(enterdvalue);

        setInputUser('');
    };

    const storeNameHandler = (event) =>{
        setInputUser(event.target.value);
       // console.log(event.target.value)

    };

    const errorHandler = () => {
        setError(null);
    }
    //{userError && <ErrorModal title={userError.title} message={userError.message}  onConfirm = {errorHandler}/>}
    return(
   
      
       <div>
           {userError && <ErrorModal title={userError.title} message={userError.message}  onConfirm = {errorHandler}/>}
           <Card className={classes.input}>
               < form onSubmit={addhandler}>

           <label htmlFor="name">Name:</label>
           <input id="name" type="text" onChange={storeNameHandler} value={inputUser} ref={userInput}/>
       
       <button type='submit'>submit</button>
       </form>
      </Card>
       
        </div>
       
  
    );
}
export default UserName;;