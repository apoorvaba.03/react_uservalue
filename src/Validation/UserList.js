import React from 'react';
import Card from '../UI/Card';
import classes from './Userlist.module.css';

const UserList = (props) =>
{
    return(
        <Card className={classes.users}>
        <ul>
            {
                props.items.map((item) => <li key={item.id}>{item.name}</li>)

            }
        </ul>
         </Card>

    );

}
export default UserList;